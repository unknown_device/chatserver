package com.pingwinno;


import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    static final int PORT = 2523;
    private org.slf4j.Logger log = LoggerFactory.getLogger(getClass().getName());
    private ArrayList<ClientHandler> clients = new ArrayList<>();
    private MessagesBuffer messageBuffer = new MessagesBuffer();

    public Server() {
        Socket clientSocket;

        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            log.info("Server is running");
            //handle incoming connections
            while (true) {
                clientSocket = serverSocket.accept();
                log.info("Incoming connection");
                ClientHandler clientHandler = new ClientHandler(clientSocket, this, messageBuffer);
                clients.add(clientHandler);
                new Thread(clientHandler).start();
                log.info("Connection established");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            log.info("Client closed");
            clients.forEach(ClientHandler::close);
        }

    }

    public void sendBroadcastMessage(String message) {
        clients.forEach(client -> client.sendMsg(message));
    }

    public void removeClient(ClientHandler client) {
        clients.remove(client);
    }
}
